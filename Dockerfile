FROM eclipse-temurin:17-jdk-alpine
WORKDIR /
#RUN mkdir -p /opt/wsil/eie/certs
#ADD src/KEYSTORE_TMO_CERT /opt/wsil/eie/certs/KEYSTORE_TMO_CERT
ADD src/CustomerManagementWSIL-V1.1.jar CustomerManagementWSIL-1.0.jar
EXPOSE 10095
ENTRYPOINT exec java -jar CustomerManagementWSIL-1.0.jar